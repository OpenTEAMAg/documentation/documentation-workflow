## Documentation template

This MkDocs template was forked from the OpenTEAM Code of Conduct project. You can use it to create a static HTML webpage of whatever documentation you save in markdown files in this repository.  

## HTML webpage information

- site_name: OpenTEAM Documentation Workflow
- site_url: https://openteamag.gitlab.io/documentation/documentation-workflow/

Now you can add markdown files to your repository and the index will match the folder structure of your files!

## Learn more about mkdocs generally

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

# Tips for using MkDocs

## Getting Started

1. Install [GitKraken](https://www.gitkraken.com/)
    - The purpose of GitKraken is to allow you to edit a GitLab repository locally (on your personal device), and then push those changes back to the master repository (accessible to all contributers). We use GitKraken because it is far more user-friendly to edit a repository on your personal device than within a GitLab browser. Additionally, two people can contribute at one time without overwriting each other.
2. Install [Visual Studio Code](https://code.visualstudio.com/)
    - The purpose of VS Code is to allow you to create and edit files within the repository on your local device. Once you've completed and saved your changes, you will see your updates tracked in GitKraken, which will allow you to push those changes back to the master.
3. Clone the repository to your local device
    - Once installed, open GitKraken and sign in with your GitLab credentials
    - Open your "repository management" window. --> Click "clone" in the lefthand directory. --> Click "GitLab.com" 
        - Next to "Where to clone to," choose a the pathway under which you'd like to save your local copy of the repository.
        - Next to "Repository to clone," search all repositories to which you have access, and clone the relevant repository.

## Contributing Locally

1. Once you have your repository cloned, you can open all files in VS Code. Open VS code, click on the explorer, and choose "Open Folder." Then follow the pathway to your cloned repository.
2. Once you have the local repository open in VS Code, you can update all files in your local repository. You can also create your own files if necessary.
    - When creating new files, be sure to save them as .md files so that they format in markdown.
    - Also be sure to save files to the 'docs' folder within your correct repository pathway. 
3. If you wish to preview your changes before pushing to the master branch of the repository, you can set up a virtual environment:
    - If this is the first time creating a virtual environment, you'll need to make sure you have the pip Python package installed, as well as the mkdocs package. You can find detailed instructions for this installation [here](https://www.mkdocs.org/user-guide/installation/).
        - Note, if you're running your virtual environment on a Mac, you'll need to start the installation process by first downloading HomeBrew and following the setup instructions for that package, then proceed with the pip and mkdocs package installations. Detailed instructions for this process can be found [here](https://suedbroecker.net/2021/01/25/how-to-install-mkdocs-on-mac-and-setup-the-integration-to-github-pages/).
    - Once that's completed, start the virtual environment by typing "mkdocs serve" into the VS code terminal. This will build the local environmental and provide a link to follow. That link will stay updated with changes that you save to files in your local environment. 
3. After saving any local changes, you will see them mirrored in track changes in GitKraken. To push those changes to the master repository in GitLab, follow these steps:
    - Click "View change" in the working directory
    - For changes that you would like to commit (save to the master repository), click "Stage File"
    - Under "Commit Message," write a brief summary and description of the changes made, then press "Commit changes to # files"
    - Once committed, go to the top tool bar in GitKraken and click the "Push" arrow (Push to origin/master).
       - If others have made changes to the master since you last worked on the repository, click  the "Pull" arrow to sync those changes with your local version. 

## Checking Contributions

After pushing a commit to from the origin to master, you will get a pop-up message saying "Push Successful". Once this happens, open your GitLab browser, go to the relevant repository, and click "CI/CD" in the lefthand directory. This will show you the history of changes made to the repository. If the pipeline status is "Passed," you're set to keep making changes. If the peopline status is "failed," your changes have not deployed to the static webpage successfully. This could be a formatting issue, a pathway issue, etc. Try backtracking to troubleshoot the issues with whatever changes you just made.

## Other Information

1. For this template, [index.md] is both a page and the home button (above the search bar in the lefthand directory). You can erase the autopopulated information and replace it with whatever you want, but be sure to keep that file for the home button.
2. Define your directory under **"Nav:"** in the file [mkdocs.yml]. Use hyphens to list all files you would like named in the directory (all deployed from the repository to the static webpage). If you would like headings that are not associated with a file (as is the case with "User Information," "Past Literature Reviews," and "Sources" on this page), you should use a follow this structure:

    nav: 

     - heading:

         - Directory Listing: 'associated file'

3. We use a MkDocs template with a ReadTheDocs theme. For more information for how to format and contribute using this theme and structure, check out [mkdocs.readthedocs.io](https://mkdocs.readthedocs.io/en/restructure-compat/)

