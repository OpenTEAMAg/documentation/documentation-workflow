# Add additional elements to Gitlab Pages

## Adding Mermaid Diagrams 


```mermaid
---
title: Ag Data Wallet Workstreams
---
graph LR
    A(Ag Data Wallet UI/UX collabathon) -->C(Consent Management)
    B(Ag Data Use Agreements) --> C
    C --> D(Ag Data Wallet)
    E(Identity Services)--> D
``` 

```mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
```

## Tables 

| Project | Modus |
|----|----|
| Name | Modus Lab Method Library |
| Short name | Modus Lab Method Library |
| Agency or Organization | OpenTEAM, Our Sci, axi lab @ Purdue U |
| Asset type | Stakeholder and service mapping |
| Model or software class | Data set |
| Link to asset source site | https://airtable.com/appcE8fUEjo5h7cMt/shrukjlgeq0gSQTaJ |
| Link to documentation | https://docs.google.com/document/d/1rWw8MKJK0z-gwL7o_p4bansqmDq8d2OZ/edit |
| Text description | Our Lab Method Library inventories lab methods and associates them to Modus IDs. Modus Agricultural Lab Test Data Standard is a data interoperability infrastructure with two components: 1) a schema structure for lab test data so that labs can send data to FMIS (farm management information systems) like farmOS / PODS, and 2) a controlled vocabulary in the form of an established \"nomenclature\" of \"modus IDs\" for all variations of every type of lab soil test in use today. |
| Point of contact & contact email | Juliet Norton jnnorton@purdue.edu, Adie Pregenzer adie@our-sci.net |
| Date of last update, version number (if available) | Synced with Modus V2 nomenclature from October 2023. We\'re one version behind (most recent Modus v2 Nomenclature release was Dec 2023). |
| Metadata |  |
| Available to the public? | Yes, read only in Airtable, downloadable exports from GitLab |
| Has API? | Airtable\'s API, must have content manager permissions (cost monies) |
| Related Assets | Modus Agricultural Lab Test Data Standard (https://aggateway.atlassian.net/wiki/spaces/MOD/overview?homepageId=3465248821), |
| Additional assets (unlinked) | Modus Slim (cannot find link, gotta ask Sam), Modus PDF converter (ask sam for link) |
| What is it built on (tech stack) | Airtable |
| What is it built with (language) | Can be exported as per-table csv, per table json. API works in python, javascript, and\.... curl? |
| System requirements | Web browser |
| License | https://creativecommons.org/licenses/by-nd/4.0/ |
| Statement on data storage, data privacy management | https://gitlab.com/OpenTEAMAg/usda-cooperative-agreements/modus/modus-soil/-/blob/master/README.md?ref_type=heads |
| Context for use, use documentation (who is this for? etc) | This resource is for anybody who is wanting to know what methods were used in their soil lab test. Soil Scientists, Data Scientists, Programmers |
| What are the dependencies? What is it related to? | https://github.com/AgGateway/Modus |
| Data needs for IRA-GHG DB (TBD) |  |
| Primary Outputs | https://gitlab.com/OpenTEAMAg/usda-cooperative-agreements/modus/modus-soil |
| Inputs Required | None, can be browsed. |
| What does this need for further development or adoption? | Needs a permanent home, funding for development, a team committed to maintaining it. |