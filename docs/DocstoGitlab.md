# Overview 

This page describes two methods for converting a Google Docs to Markdown and inputting these files to a Gitlab project to publish to a static site.

1.  Using PanWriter: a free and open source software that requires a download and a couple additional steps

2.  Using the Docs to Markdown Extension: Requires adding an extension to a Google profile and has a limit to the amount of free conversions

This process assumes you already have a Gitlab Repo and static site. see this page for setting up a static site in Gitlab: [[https://openteamag.gitlab.io/documentation/template/faq/]{.underline}](https://openteamag.gitlab.io/documentation/template/faq/)

# Writing Google Docs for Markdown Pages

Within \[the folder\] there are a variety of templates for creating pages. Each project will need a landing page, the rest are up to you and the scope of your project. Duplicate templates (or the entire folder) that you need and build out your pages. Follow the formatting and styling that exists in each.

\[add an overview of our styling - header hierarchy, etc\]

# Using PanWriter and Pandocs 

*Pros: Free and open source, customizable*

*Cons: Requires software instillation and has the additional step of downloading a Google Docs to a .docx file*

By following these steps, you can easily convert a Google Doc to Markdown text using PanWriter and Pandoc. PanWriter offers a user-friendly interface for conversion, while Pandoc provides a powerful command-line alternative. These softwares allow a variety of conversions and functions beyond what is described here.

## Download PanWriter and Pandocs (one time installation) 

Navigate to [[PanWriter\'s official website]{.underline}](https://panwriter.com/). Find the download link suitable for your operating system (Windows, macOS, or Linux) and click on it to download the installation file. Follow installation instructions. PanWriter is free and open source software.

You also have to install Pandoc to export to most formats. Navigate to the [[installing pandoc]{.underline}](https://pandoc.org/installing.html) instructions, find the download link suitable for your operating system (Windows, macOS, or Linux) and click on it to download the installation file.

## Download the Google Doc as a .docx File

Open the file you wish to convert to Markdown.

-   Click on File in the top left corner.

-   Hover over Download.

-   Click on Microsoft Word (.docx).

This will download your Google Doc as a .docx file to your computer.

## Convert the .docx File to Markdown using PanWriter

-   Launch PanWriter on your computer.

-   **Open the .docx File in PanWriter**: In PanWriter, click on File \> Open and select the .docx file you downloaded from Google Docs.

-   **Convert to Markdown**: Click on File \> Export \> Markdown. Choose a location on your computer to save the Markdown file and click Save.

## *Alternative Method: Using Pandoc from the Command Line*

-   **Open Command Prompt or Terminal**: On Windows, press Win + R, type cmd, and press Enter. On macOS or Linux, open your Terminal application.

-   **Navigate to the Directory of the .docx File**: Use the cd command to change directories to where your .docx file is located.

cd /path/to/your/file

-   **Run Pandoc to Convert the File**: Use the following command to convert the .docx file to Markdown:

pandoc -s yourfile.docx -o yourfile.md

Replace yourfile.docx with the name of your downloaded Google Doc file and yourfile.md with the desired name for your Markdown file.

# Using the [[Docs to Markdown]{.underline}](https://workspace.google.com/marketplace/app/docs_to_markdown_pro/483386994804) extension

*Pros: Simple Google extension that works directly in Google Docs. Supports image export*

*Cons:* limited amount of free conversions, not free or open source

## Install the Docs to Markdown Extension 

[[Docs™ to Markdown Pro - Google Workspace Marketplace]{.underline}](https://workspace.google.com/marketplace/app/docs_to_markdown_pro/483386994804)

## Convert the Google Docs to Markdown

In the Google Doc, click Extensions \> Docs to Markdown Pro \> Convert Docs to Markdown or HTML

![](media/image5.png){width="6.5in" height="3.4722222222222223in"}

Select "bundle MD & images as a zip file and the "To Markdown"

![](media/image4.png){width="3.2864588801399823in" height="5.842592957130359in"}

Select "View Google Drive Zip File to see the output. Download this Zip file and 'unzip'.

## Markdown to Gitlab

Within the downloaded folder, you will see an 'Output.md' and an 'Images' folder.

For the markdown file, either:

1.  Drag and drop Output.md into the 'docs' folder.

![](media/image2.png){width="4.516233595800525in" height="6.130208880139983in"}

OR

2.  Add a markdown file and copy and paste the contents of the output.md into this file.

![](media/image1.png){width="4.411458880139983in" height="2.1279625984251966in"}

## Adding the Images

Create an "Images" Folder and drag and drop the images into this folder.

*Note that the 'img' folder will not work with this workflow, please use "Images"*

## Adding pages in the YML file

In mkdocs.yml, add the new pages under 'nav:' using the following syntax:

- what you want the tab to be called: 'Name_of_file.md'



![](media/image3.png){width="6.5in" height="2.5277777777777777in"}


