# Process

## Background
This (Event)  happened as part of an ongoing collaboration between teams at (example) OpenTEAM, and other partner organizations in our ecosystem. We identified a need based on (thing) and (inciting event). Some of those workstreams include: 


- [Example](https://www.our-sci.net/the-common-profile/) and [Example](https://www.our-sci.net/the-common-profile/) work by TEAM




## Timeline

*Date* - Description

- Gitlab issue: [Link](https://gitlab.com/OpenTEAMAg/feedback/support_requests/-/issues/170)

<br>


*Date* - Description

 * Questions:

> * How?
> * How have those processes and needs changed?
> * What? 

* Identified need for (thing). 

<br>



### Summary and documentation



**Documents**

- [Example](https://drive.google.com/drive/u/1/folders/1oW3AP76SzMIQauV8T0iWjvwIJTj7Heep)



*Follow-up meeting scheduled for January 22nd, 2024* 