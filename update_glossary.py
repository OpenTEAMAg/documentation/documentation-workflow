import yaml
import requests
import os

# URL to the main glossary YAML file in the central repository
GLOSSARY_URL = 'https://gitlab.com/OpenTEAMAg/documentation/glossary/-/raw/521ce4acf6d8dff1ea5043066cb84dd2574a413e/glossary.yaml'

# Terms needed for the specific project
PROJECT_TERMS = [
    "API",
    "CI/CD"
]

def fetch_glossary(url):
    response = requests.get(url)
    if response.status_code == 200:
        return yaml.safe_load(response.content)
    else:
        raise Exception(f"Failed to fetch glossary: {response.status_code}")

def generate_project_glossary(main_glossary, terms, output_path):
    glossary_terms = []
    for item in main_glossary['terms']:
        if item['term'] in terms:
            glossary_terms.append(item)
    
    with open(output_path, 'w') as file:
        # Write front matter
        file.write('---\n')
        file.write(f'title: Project Glossary\n')
        file.write('---\n\n')
        
        # Write glossary content
        file.write('# Project Glossary\n\n')
        current_letter = ''
        for item in glossary_terms:
            first_letter = item['term'][0].upper()
            if first_letter != current_letter:
                current_letter = first_letter
                file.write(f'## {current_letter}\n')
            file.write(f'### {item["term"]}\n')
            file.write(f'**Definition:** {item["definition"]}\n\n')

def main():
    main_glossary = fetch_glossary(GLOSSARY_URL)
    output_path = 'docs/glossary.md'
    generate_project_glossary(main_glossary, PROJECT_TERMS, output_path)

if __name__ == '__main__':
    main()
